#!/bin/sh
set -e

meson build
ninja -C build test
rm -rf build
